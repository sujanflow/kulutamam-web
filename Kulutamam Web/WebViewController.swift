//
//  ViewController.swift
//  Kulutamam Web
//
//  Created by Tanvir Palash on 1/8/18.
//  Copyright © 2018 Tanvir Palash. All rights reserved.
//

import UIKit
import WebKit


class WebViewController: UIViewController,WKNavigationDelegate,UITableViewDelegate,UITableViewDataSource {
    
    

    var webUrl:String = "https://www.kulutamam.com/?set-lang=\(UserDefaults.standard.string(forKey: "lan") ?? "en")"
    
    var lanArray = [Dictionary<String,Any>]()
    var currencyArray = [String]()
    
    //https://www.kulutamam.com/?set-lang=ar-ae
    //https://www.kulutamam.com/?set-lang=en
   
    @IBOutlet weak var topNavButton: UIButton!
    //@IBOutlet var containerWebView: WebView!
    
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var lanImageView: UIImageView!
    
    @IBOutlet weak var currencyLabel: UILabel!
    
    @IBOutlet weak var languageTableView: UITableView!
    
    @IBOutlet weak var currencyTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // print("web url",webUrl);
        lanArray.append(["title":"English".localized, "icon":"uk_icon"])
        lanArray.append(["title":"Arabic".localized, "icon":"ar_icon"])
        
        currencyArray = ["USD($)","AED(د.إ)","KWD(د.ك)","SAR(ر.س)"]
        
        self.webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)

        self.webView.navigationDelegate=self;
        self.webView.scrollView.bounces=false;
        
        
        addGradient()
        
//        NotificationCenter.default.addObserver(self, selector: #selector(refreshList), name: NSNotification.Name(rawValue: "reload"), object: nil)
//        
        print("UserDefaults.standard.integer",UserDefaults.standard.integer(forKey: "index"))
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        if UserDefaults.standard.integer(forKey: "index") == 0 {
            
            self.loadWebViewWithUrl(string: webUrl);
            
        }else if UserDefaults.standard.integer(forKey: "index") == 1{
            
            self.loadWebViewWithUrl(string: "https://www.kulutamam.com/inspire-me.aspx");
            
        }else if(UserDefaults.standard.integer(forKey: "index") == 2){
            
            loadWebViewWithUrl(string: "https://www.kulutamam.com/info/about-us.aspx")
            
        }else if(UserDefaults.standard.integer(forKey: "index") == 3){
            
            loadWebViewWithUrl(string: "https://www.kulutamam.com/info/faq.aspx")
            
        }else if(UserDefaults.standard.integer(forKey: "index") == 4){
            
            loadWebViewWithUrl(string: "https://www.kulutamam.com/info/contact-us.aspx")
        }


        
        languageTableView.delegate = self
        languageTableView.dataSource = self
        languageTableView.layer.masksToBounds = true
        languageTableView.layer.borderColor = UIColor.lightGray.cgColor
        languageTableView.layer.borderWidth = 1.0
        
        currencyTableView.delegate = self
        currencyTableView.dataSource = self
        currencyTableView.layer.masksToBounds = true
        currencyTableView.layer.borderColor = UIColor.lightGray.cgColor
        currencyTableView.layer.borderWidth = 1.0
        
        
        if Language.language == Language.english{
            
            self.lanImageView.image = UIImage(named: "uk_icon")
            
        }else{
            
            self.lanImageView.image = UIImage(named: "ar_icon")
            
        }
        
        currencyLabel.text = UserDefaults.standard.string(forKey: "currency")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
     //   insertContentsOfCSSFile(into: self.webView) // 2

    }
    
    func addGradient() {
        
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [UIColor("#3c004b").cgColor, UIColor("#33ccff").cgColor]
        
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.topView.frame.size.width+50, height: self.topView.frame.size.height)
        
        self.topView.layer.insertSublayer(gradient, at: 0)

    }
    
    func loadWebViewWithUrl(string: String) {
        
        print("url string",string)
        
        let url = URL(string:string)
//        let request = NSURLRequest(url:url! as URL)
      //  print("request",request)
       //  self.containerWebView.load(URLRequest(url: url! as URL))
        
       let request = URLRequest(url: url!)

       self.webView.load(request)

    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
      //  webView.alpha=0.0;
        
        print("didStartProvisionalNavigation")
        
       
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        
       

//       insertContentsOfCSSFile(into: webView) // 2


    }
    
    
    
    
    func insertContentsOfCSSFile(into webView: WKWebView) {
        
        
    //    var loadStyles = "var script = document.createElement('link'); script.type = 'text/css'; script.rel = 'stylesheet'; script.href = 'http://fake-url/styles.css'; document.getElementsByTagName('body')[0].appendChild(script);"
        

        guard let path = Bundle.main.path(forResource: "custom-style", ofType: "css") else { return }
        let css = try! String(contentsOfFile: path).replacingOccurrences(of: "\\n", with: "", options: .regularExpression)
       // let cssString = try? String(contentsOfFile: path).components(separatedBy: .newlines).joined()
        let js = "var style = document.createElement('style'); style.innerHTML = '\(css)'; document.head.appendChild(style);"
        
        webView.evaluateJavaScript(js)
        
      //  print("js",js)
        
        webView.alpha=1.0;
        
        

        
    }

    
    func webView(webView: WKWebView!, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError!) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
//        if self.containerWebView.canGoBack {
//            self.containerWebView.goBack()
//        }
//        else
//        {
//            self.loadWebViewWithUrl(string: webUrl);
//
//        }
        if let drawerController = navigationController?.parent as? KYDrawerController {
            if Language.language == Language.english{
                
                drawerController.drawerDirection = .left
                drawerController.setDrawerState(.opened, animated: true)
                
            }else{
                
                drawerController.drawerDirection = .right
                drawerController.setDrawerState(.opened, animated: true)
                
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "loading") {
            //  backButton.enabled = webView.canGoBack
            //  forwardButton.enabled = webView.canGoForward
        }
        if (keyPath == "estimatedProgress") {
            
         //   print("observeValue estimatedProgress",self.webView.estimatedProgress)
            
            if self.webView.estimatedProgress >= 0.85 {

               insertContentsOfCSSFile(into: self.webView) // 2

            }
            
            progressView.isHidden = self.webView.estimatedProgress == 1
            progressView.setProgress(Float(self.webView.estimatedProgress), animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == languageTableView {
            
            return 2
            
        }else if tableView == currencyTableView{
            
            return 4
        }
        
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == languageTableView {
            
        
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "lanCell")!
            
            let titlelabel:UILabel = cell.viewWithTag(101) as! UILabel
            
            titlelabel.text = lanArray[indexPath.row]["title"]! as? String
            
            let imageView:UIImageView = cell.viewWithTag(102) as! UIImageView
            
            imageView.image = UIImage(named: lanArray[indexPath.item]["icon"]! as! String)
            
            
            return cell
        }else {
            
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "currencyCell")!
            
            cell.textLabel?.font = UIFont(name:"Lato-Regular",size:13)
            cell.textLabel?.text = currencyArray[indexPath.row]
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == languageTableView{
            
            if indexPath.row == 0 {
                
                UserDefaults.standard.set("en", forKey: "lan")
                
                // webUrl = "https://www.kulutamam.com/?set-lang=en"
                
                
                Language.language = Language.english
                
                
            }else if indexPath.row == 1{
                
                UserDefaults.standard.set("ar-ae", forKey: "lan")
                
                //  webUrl = "https://www.kulutamam.com/?set-lang=ar-ae"
                
                self.lanImageView.image = UIImage(named: "ar_icon")
                
                Language.language = Language.arabic
                
            }
            self.loadWebViewWithUrl(string: webUrl)
            
            languageTableView.isHidden = true
            

        }else{
            //["USD($)","AED(د.إ)","KWD(د.ك)","SAR(ر.س)"]
            if indexPath.row == 0{
               
                UserDefaults.standard.set("USD($)", forKey: "currency")
                
                 webUrl = "https://www.kulutamam.com/?set-currency=USD"
                
                
            }else if indexPath.row == 1{
                UserDefaults.standard.set("AED(د.إ)", forKey: "currency")
                
                webUrl = "https://www.kulutamam.com/?set-currency=AED"
                
            }else if indexPath.row == 2{
                UserDefaults.standard.set("KWD(د.ك)", forKey: "currency")
                
                webUrl = "https://www.kulutamam.com/?set-currency=KWD"
            }else if indexPath.row == 3{
                
                UserDefaults.standard.set("SAR(ر.س)", forKey: "currency")
                
                 webUrl = "https://www.kulutamam.com/?set-currency=SAR"
            }
            currencyLabel.text = currencyArray[indexPath.row]
            self.loadWebViewWithUrl(string: webUrl)
            
            currencyTableView.isHidden = true
            
        }
    }
    
    @IBAction func changeLanguageButtonAction(_ sender: UIButton) {
       
       currencyTableView.isHidden = true
       languageTableView.isHidden = !languageTableView.isHidden
        
    }
    
    
    @IBAction func changeCurrencyButtonAction(_ sender: Any) {
        
        languageTableView.isHidden = true
        currencyTableView.isHidden = !currencyTableView.isHidden
        
    }
    
    
}
