//
//  DrawerMenuViewController.swift
//  Kulutamam
//
//  Created by Md.Ballal Hossen on 19/12/18.
//  Copyright © 2018 Tanvir Palash. All rights reserved.
//

import UIKit

class DrawerMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    

    
    @IBOutlet weak var menuTableView: UITableView!
    
    var menuArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        menuArray = ["HOME".localized,"INSPIRE ME".localized,"ABOUT US".localized,"FAQS".localized,"CONTACT US".localized]
        
        menuTableView.delegate = self
        menuTableView.dataSource = self
        
       
        menuTableView.selectRow(at: IndexPath(row:0, section: 0), animated: false, scrollPosition: .none)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        print("viewDidAppear in drawer")
        
        let indexPath = IndexPath(row: UserDefaults.standard.integer(forKey: "index"), section: 0)
        
        menuTableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        
        

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "menuCell")!
        
        if Language.language == Language.arabic {
            
            cell.textLabel?.textAlignment = .right
        }else{
            
            cell.textLabel?.textAlignment = .left
        }
        cell.textLabel?.font = UIFont(name:"Lato-Regular",size:14)
        cell.textLabel?.text = menuArray[indexPath.row]
        
        print("indexPath.row selected",indexPath.row)
        
        
        if indexPath.row == 0 {
            
            cell.setSelected(true, animated: false)
            
        }
        cell.textLabel?.textColor = UIColor("#33ccff")
       // cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        if cell.isSelected {
            
            
            cell.textLabel?.textColor = UIColor("#C0CF19")
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let elDrawer = navigationController?.parent as? KYDrawerController
        let webController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
        var navController: UINavigationController? = nil
        
        if let aController = webController {
            
            navController = UINavigationController(rootViewController: aController)
        }
        
        let index = indexPath.row
        
        UserDefaults.standard.set(index, forKey: "index")
        
       
        
//        let dataDic = [ "index":index ]
//
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reload"), object: dataDic)
        
        elDrawer?.mainViewController = navController
        elDrawer?.setDrawerState(.closed, animated: true)
        
        
        print("root",UIApplication.shared.keyWindow?.rootViewController! as Any)
        
        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.textLabel?.textColor = UIColor("#C0CF19")
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.textLabel?.textColor = UIColor("#33ccff")
    }
    

}


